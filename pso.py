#!/usr/bin/python3
# -*- coding: utf-8 -*-
import abc,copy
from abc import abstractmethod
import random
#TODO: iniciar particulas
#TODO: calcular FO de cada particula
#TODO: elegir mejor posicicón de cada particula
#TODO: actualizar mejor conocida en g
#TODO: iniciar velocidad de cada particula 
'''
v_i ~ U(-|max-min|,|max-min|)
'''
#TODO: criterio de parada(iteraciones o convergencia)

##algoritmo
#por cada particula
	#por cada dimensión(Feature)
		#random rp, rg € [0,1]
		#actualizar la velocidad de la particula V_i = Wv_i,d + F_p r_p(p_i-x_i) + F_g r_g (gd -xi,d)
	#actualizar posición de la particula x_i = x_i+v_i
	#si FO(x_i) es mejor que FO(pi):
		#pi = x_i
		#si FO(pi) es mejor que FO(g) actualizar g
			
class Particula(abc.ABC):
	def __init__(self,**bounds):
		self.bounds = bounds
		
	def init_particle(self):
		pos_features = dict()
		vel_features = dict()
		for f, bound in self.bounds.items():
			pos_features[f] = random.uniform(*bound)
			vel_features[f] = .3*random.uniform(-(max(bound)-min(bound)),max(bound)-min(bound))
		self.position = Position(self.bounds,**pos_features)
		self.particle_best = Position(self.bounds,**pos_features)
		self.velocity = Position(self.bounds,**vel_features)
		
		return self
		
	def clone(self):
		return copy.deepcopy(self)

	def update_position(self):

		self.position += self.velocity
		return self.position,self.particle_best

	@abstractmethod
	def update_velocity(self,global_best):
		raise NotImplementedError()
	
	def update_best_pos(self,new_best):
		self.best_pos = copy.deepcopy(new_best)
		
class Position(object):
	def __init__(self,bounds,**kwargs):
		self.features = kwargs
		self.bounds = bounds
		for k,v in kwargs.items():
			setattr(self, k, v)
	def Set(self,k,v):
		self.features[k]=v
		setattr(self, k, v)
	def __iadd__(self,other):
		for k in self.features.keys():
			self.features[k]+=other.features[k]
			if self.features[k] < min(self.bounds[k]):
				self.features[k] = min(self.bounds[k])
			elif self.features[k] > max(self.bounds[k]):
				self.features[k] = max(self.bounds[k])
		for k,v in self.features.items():
			setattr(self, k, v)
		return copy.deepcopy(self)

class ObjetiveFunction(abc.ABC):
	@abstractmethod
	def value(self,**kwargs):
		raise NotImplementedError()

	@abstractmethod
	def comparation(self,A,B):
		raise NotImplementedError()

	def Is(self,**kwargs):
		self.saved_value = self.value(**kwargs)
		return copy.deepcopy(self)

	def better_than(self,**kwargs):
		return self.comparation(self.saved_value,self.value(**kwargs))

class Enjambre(object):
	particles = []
	
	def __init__(self,template_particle ,objetive_function,population=100,max_iterations=1000):
		for _ in range(population):
			self.particles.append(template_particle.clone().init_particle())
		self.max_iterations = max_iterations
		self.current_iteration = 0
		self.objetive_function = objetive_function
		self.global_best = self.particles[0].particle_best
		for particle in self.particles:
			if self.objetive_function.Is(**particle.particle_best.features).better_than(**self.global_best.features):
				self.global_best = copy.deepcopy(particle.particle_best)


	
	def __iter__(self):
		return self
	# Python 3 compatibility
	def __next__(self):
		return self.next()

	def next(self):
		if self.current_iteration < self.max_iterations:
			self.current_iteration += 1 
			return self.algorithm()
		else:
			raise StopIteration()
	
	def algorithm(self):

		for particle in self.particles:
			particle.update_velocity(self.global_best)
			actual_pos , best_pos = particle.update_position()
			if self.objetive_function.Is(**actual_pos.features).better_than(**best_pos.features):
				particle.update_best_pos(actual_pos)
				if self.objetive_function.Is(**actual_pos.features).better_than(**self.global_best.features):
					self.global_best = copy.deepcopy(actual_pos)
		return self.objetive_function.value(**self.global_best.features),self.global_best
	
