import httplib
class HbaseAPIConsummer(object):
    def __init__(self,host,port=20550,table=None,ssl = False, verify=True):
        self.host=host
        self.port=port
        self._table=table
        self.ssl = ssl
        self.verify = verify
        self.many = None
        self.manyCount=0

    def table(self,table):
        return HbaseAPIConsummer(self.host,self.port,table,self.ssl,self.verify)

    def row(self,key,col=None):
        import requests,json,base64
        url = "http%s://%s:%d/%s/%s"%("s" if self.ssl else "",self.host, self.port, self._table,key)
        if col:
            url += "/"+col
        headers = {'Accept': "application/json"}
        response = requests.request("GET", url, headers=headers,verify=False)
        row = json.loads(response.text)["Row"][0]
        return {base64.b64decode(c["column"]):base64.b64decode(c["$"]) for c in row["Cell"]}

    def put(self, key, hb_values):
        if self._table:
            import xml.etree.ElementTree as ET
            import base64
            import requests
            root = ET.fromstring('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><CellSet></CellSet>')
            for col,value in hb_values.items():
                row = ET.SubElement(root, 'Row')
                row.attrib.update({"key":base64.encodestring(key)})
                cell = ET.SubElement(row, 'Cell')

                cell.attrib.update({"column":base64.encodestring(col)})
                if type(value) == unicode:
                    cell.text = base64.encodestring(value.encode("utf-8"))
                else:
                    cell.text = base64.encodestring(value)

            url = "http%s://%s:%d/%s/fakerow"%("s" if self.ssl else "",self.host, self.port, self._table)
            payload = ET.tostring(root)
            headers = {
                'Accept': "text/xml",
                'Content-Type': "text/xml"
                }
            response = requests.request("PUT", url, data=payload, headers=headers,verify=self.verify)
            print "hbase satus: ", response.status_code
            if response.status_code > 399:
                raise Exception("Error realizar el put : %d@%s '%s'\n%s\n%s"%(response.status_code,httplib.respo
nses[response.status_code],response.content,url,payload) )
        else:
            raise Exception("Tabla no especificada")

    def putMany(self, key, hb_values, autoflush=20):
        if self._table:
            import xml.etree.ElementTree as ET
            import base64

            if self.many is None:
                self.many = ET.fromstring('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><CellSet></Cel
lSet>')

            for col,value in hb_values.items():
                 if value is not None:
                    row = ET.SubElement(self.many, 'Row')
                    row.attrib.update({"key":base64.encodestring(key)})
                    cell = ET.SubElement(row, 'Cell')

                    cell.attrib.update({"column":base64.encodestring(col)})
                    if type(value) == unicode:
                        cell.text = base64.encodestring(value.encode("utf-8"))
                    else:
                        cell.text = base64.encodestring(value)
            self.manyCount+=1
            if self.manyCount >= autoflush:
                self.flushMany()

        else:
            raise Exception("Tabla no especificada")

    def flushMany(self):
        import xml.etree.ElementTree as ET
        import requests
        url = "http%s://%s:%d/%s/fakerow"%("s" if self.ssl else "",self.host, self.port, self._table)
        if self._table:
            if self.many:
                payload = ET.tostring(self.many)
                headers = {
                    'Accept': "text/xml",
                    'Content-Type': "text/xml"
                    }

                response = requests.request("PUT", url, data=payload, headers=headers,verify=self.verify)

                #prepped = response.prepare()
                #s = requests.Session()
                #resp = s.send(prepped)

                print "hbase satus: ", response.status_code
                if response.status_code > 399:
                    raise Exception("Error realizar el put : %d@%s '%s'\n%s\n%s"%(response.status_code,httplib.r
esponses[response.status_code],response.content,url,payload) )
                else:
                    self.manyCount=0
                    self.many=None
                    return True
        else:
            raise Exception("Tabla no especificada")