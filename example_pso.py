import pso,math,random
from matplotlib import pyplot as plt
import numpy as np
from matplotlib import cm

from mpl_toolkits.mplot3d import Axes3D

class FO(pso.ObjetiveFunction):
    def value(self,**kwargs):
        x = kwargs["x"]
        y = kwargs["y"]
        return 7*x*y/(math.exp(x**2+y**2))
    def comparation(self,A,B):
        return A<B

class ParticulaEjemplo(pso.Particula):
    def update_velocity(self,global_best):
        #actualizar la velocidad de la particula V_i = Wv_i,d + F_p r_p(p_i-x_i) + F_g r_g (gd -xi,d)
        r = random.uniform(0,.3)
        for d in self.velocity.features.keys():
            
            vi = (1-r)*self.velocity.features[d]+0.0*random.uniform(0,1)*(self.particle_best.features[d]-self.position.features[d]) + r*(global_best.features[d]-self.position.features[d])
            
            self.velocity.Set(d,vi)

if __name__ == "__main__":
    fig = plt.figure()
    fig.suptitle('z=\\frac{7xy}{e^{x^2+y^2}}', fontsize=16)
    X = np.linspace(-2,2,1000)
    Y = np.linspace(-2,2,1000)
    X, Y = np.meshgrid(X, Y)
    ax = fig.gca(projection='3d')
    Z = np.divide(7*np.multiply(X,Y), np.exp(np.power(X,2)+np.power(Y,2)))
    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
    plt.savefig("images_3/surface.png",format="png")
    plt.show()

if __name__ == "__main___":
    fo = FO()
    x_bounds,y_bounds = [-2,2],[-2,2]
    template = ParticulaEjemplo(x=x_bounds, y=y_bounds)
    PSO = pso.Enjambre(template,fo,max_iterations=50,population=500)
    fig = plt.figure()
    fig.suptitle('z=\\frac{7xy}{e^{x^2+y^2}}', fontsize=16)
    ax = fig.add_subplot(111,projection='3d')
    ax.set_xlim(x_bounds)
    ax.set_ylim(y_bounds)
    ax.set_zlim([-2,2])          
    for particle in PSO.particles:
        ax.scatter(particle.position.x,particle.position.y,fo.value(**particle.position.features),  marker='o')
    plt.savefig("images_3/pso_0.png",format="png")
    plt.clf()
    for value,point in PSO:
        fig.suptitle('z=\\frac{7xy}{e^{x^2+y^2}}', fontsize=16)
        ax = fig.add_subplot(111,projection='3d')
        ax.set_xlim(x_bounds)
        ax.set_ylim(y_bounds)
        ax.set_zlim([-2,2])  
        print(value)     
        for particle in PSO.particles:
            ax.scatter(particle.position.x,particle.position.y,fo.value(**particle.position.features),  marker='o')
        #plt.show()
        plt.savefig("images_3/pso_{}.png".format(PSO.current_iteration),format="png")
        plt.clf()
